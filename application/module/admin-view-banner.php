<?php
$app->get('/admin/view/banner', function () use ($use) {
    session("user_id");
    $banner_param = (object) ['results' => [], 'found'=>true];
    $use->app->applyHook('banner', $banner_param);
    $use->template->prepare('admin-banner.html');
    $use->template->param("users", $use->users);
    $use->template->param("banner", $banner_param->results["banner"]);
    $use->template->execute();
})->name('admin-banner');
$app->hook('banner', function ($param) use ($use){
    $query = "
        select * from carousel
    ";
    $select = $use->db->prepare($query);
    if($select->execute()){
        $result["banner"]=$select->fetchAll(PDO::FETCH_ASSOC);
        $result["count"]=$select->rowCount();
        $param->results=$result;
        if($select->rowCount() == 0){
            $param->found=true;
        }else{
            $param->found=false;
        }
        return $result;
    }
});

//insert
$app->get('/admin/add/banner', function () use ($use) {
    session("user_id");
    $use->template->prepare('admin-add-banner.html');
    $use->template->param("users", $use->users);
    $use->template->execute();
})->name('admin-add-banner');
$app->post('/admin/add/banner', function () use ($use) {
    if(isset($_FILES["banner_image"])){
        if ($_FILES["banner_image"]["type"] == "image/bmp" || $_FILES["banner_image"]["type"] == "image/png" || $_FILES["banner_image"]["type"] == "image/jpeg"){
            $user_id = $use->app->request->post('user_id');
            $banner_status = ($use->app->request->post('banner_status') == "" ? "visible" : "hidden");

            $ext = end((explode(".", $_FILES['banner_image']['name'])));
            $sourcePath = $_FILES['banner_image']['tmp_name'];
            $file = 'banner-'.date("-Y-m-d-H-i-s-").'.'.$ext;
            $targetPath = "public/data/banner/".$file; // Target path where file is to be stored
            move_uploaded_file($sourcePath, $targetPath) ; // Moving Uploaded file
            $insert = $use->db->prepare("
                insert into carousel
                values('', :banner_status, :banner_date, :banner_image);
            ");
            $insert->bindParam(':banner_status', $banner_status, PDO::PARAM_STR);
            $insert->bindParam(':banner_date', date('Y-m-d h:i:s', time()), PDO::PARAM_STR);
            $insert->bindParam(':banner_image', $file, PDO::PARAM_STR);
            if($insert->execute()){
                $banner_id = $use->db->lastInsertId();
                $use->db->exec("insert into do_carousel values('', '".$user_id."', '".$banner_id."', 'add', '".date('Y-m-d h:i:s', time())."')");
                $use->app->redirect($use->app->urlFor('admin-banner'));
            }else{
                echo "lala";
            }
        }
    }
});

//edit
$app->get('/admin/view/banner/:banner_id', function ($banner_id='$') use ($use) {
    session("user_id");
    $use->template->prepare('admin-add-banner.html');
    $use->template->param("users", $use->users);
    $use->tepmlate->param("banner", $use->db->query("select * from carousel where carousel_id='".$banner_id."'"));
    $use->template->execute();
});
?>
