<?php
$app->get('/admin/view/user', function () use ($use) {
    session("user_id");
    $use->template->prepare('admin-user.html');
    $use->template->param("users", $use->users);
    $use->template->execute();
})->name('admin-user');
?>
