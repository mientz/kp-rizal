<?php
// view
$app->get('/admin/view/article(/:title)', function ($title='$') use ($use) {
    session("user_id");

    $article_param = (object) ["article_search"=>$title, 'results' => [], 'found'=>true];
    $use->app->applyHook('article', $article_param);

    $use->template->prepare('admin-article.html');
    $use->template->param("users", $use->users);
    $use->template->param("article", $article_param->results["article"]);
    $use->template->execute();
})->name('admin-article');
$app->hook('article', function ($param) use ($use) {
    $article_search=$param->article_search;
    $query = "
        select * from article ".($article_search != '$' ? "where article_title like CONCAT('%', :article_title '%')" : "")."
    ";
    $select = $use->db->prepare($query);
    $select->bindParam(':article_title', $article_search, PDO::PARAM_STR);
    if($select->execute()){
        $result["article"]=$select->fetchAll(PDO::FETCH_ASSOC);
        $result["count"]=$select->rowCount();
        $param->results=$result;
        if($select->rowCount() == 0){
            $param->found=true;
        }else{
            $param->found=false;
        }
        return $result;
    }
});

//add
$app->get('/admin/add/article', function () use ($use) {
    session("user_id");
    $use->template->prepare('admin-add-article.html');
    $use->template->param("users", $use->users);
    $use->template->execute();
})->name('admin-add-article');
$app->post('/admin/add/article', function () use ($use) {
    if(isset($_FILES["article_image"])){
        if ($_FILES["article_image"]["type"] == "image/bmp" || $_FILES["article_image"]["type"] == "image/png" || $_FILES["article_image"]["type"] == "image/jpeg"){
            $user_id = $use->app->request->post('user_id');
            $article_title = $use->app->request->post('article_title');
            $article_text = $use->app->request->post('article_text');
            $article_type = $use->app->request->post('article_type');

            $ext = end((explode(".", $_FILES['article_image']['name'])));
            $sourcePath = $_FILES['article_image']['tmp_name'];
            $file = 'article-'.$article_title."-".date("-Y-m-d-H-i-s-").'.'.$ext;
            $targetPath = "public/data/article/".$file; // Target path where file is to be stored
            move_uploaded_file($sourcePath, $targetPath) ; // Moving Uploaded file
            $insert = $use->db->prepare("
                insert into article
                values('', :article_title, :article_date, :article_text, :article_image, '',  :article_type);
            ");
            $insert->bindParam(':article_title', $article_title, PDO::PARAM_STR);
            $insert->bindParam(':article_date', date('Y-m-d h:i:s', time()), PDO::PARAM_STR);
            $insert->bindParam(':article_text', $article_text, PDO::PARAM_STR);
            $insert->bindParam(':article_image', $file, PDO::PARAM_STR);
            $insert->bindParam(':article_type', $article_type, PDO::PARAM_STR);
            if($insert->execute()){
                $article_id = $use->db->lastInsertId();
                $use->db->exec("insert into do_article values('', '".$user_id."', '".$article_id."', 'add', '".date('Y-m-d h:i:s', time())."')");
                $use->app->redirect($use->app->urlFor('admin-article'));
            }else{
                echo "lala";
            }
        }
    }
});

//edit
$app->get('/admin/edit/article/:article_id', function ($article_id) use ($use) {
    session("user_id");
    $use->template->prepare('admin-edit-article.html');
    $use->template->param("users", $use->users);
    $use->template->param("article", $use->db->query("select * from article where article_id='".$article_id."'")->fetch(PDO::FETCH_ASSOC));
    $use->template->execute();
});
$app->post('/admin/edit/article', function () use ($use) {
    $user_id = $use->app->request->post('user_id');
    $article_id = $use->app->request->post('article_id');
    $article_title = $use->app->request->post('article_title');
    $article_text = $use->app->request->post('article_text');
    $article_type = $use->app->request->post('article_type');

    if(isset($_FILES["article_image"]) && $_FILES['article_image']['tmp_name'] != ""){
        if ($_FILES["article_image"]["type"] == "image/bmp" || $_FILES["article_image"]["type"] == "image/png" || $_FILES["article_image"]["type"] == "image/jpeg"){

            $ext = end((explode(".", $_FILES['article_image']['name'])));
            $sourcePath = $_FILES['article_image']['tmp_name'];
            $file = 'product-'.$product_name."-".date("-Y-m-d-H-i-s-").'.'.$ext;
            $targetPath = "public/data/product/".$file; // Target path where file is to be stored
            move_uploaded_file($sourcePath, $targetPath) ; // Moving Uploaded file
            $insert = $use->db->prepare("
                update article
                set
                    article_title=:article_title,
                    article_text=:article_text,
                    article_image=:article_image,
                    article_type=:article_type
                where
                    article_id=:article_id
            ");
            $insert->bindParam(':article_id', $article_id, PDO::PARAM_INT);
            $insert->bindParam(':article_title', $article_title, PDO::PARAM_STR);
            $insert->bindParam(':article_text', $article_text, PDO::PARAM_STR);
            $insert->bindParam(':article_image', $file, PDO::PARAM_STR);
            $insert->bindParam(':article_type', $article_type, PDO::PARAM_STR);
            if($insert->execute()){
                $use->db->exec("insert into do_article values('', '".$user_id."', '".$article_id."', 'edit', '".date('Y-m-d h:i:s', time())."')");
                $use->app->redirect($use->app->urlFor('admin-article'));
            }else{
                echo "lala";
            }
        }
    }else{
        $insert = $use->db->prepare("
                update article
                set
                    article_title=:article_title,
                    article_text=:article_text,
                    article_type=:article_type
                where
                    article_id=:article_id
            ");
        $insert->bindParam(':article_id', $article_id, PDO::PARAM_INT);
        $insert->bindParam(':article_title', $article_title, PDO::PARAM_STR);
        $insert->bindParam(':article_text', $article_text, PDO::PARAM_STR);
        $insert->bindParam(':article_type', $article_type, PDO::PARAM_STR);
        if($insert->execute()){
            $use->db->exec("insert into do_article values('', '".$user_id."', '".$article_id."', 'edit', '".date('Y-m-d h:i:s', time())."')");
            $use->app->redirect($use->app->urlFor('admin-article'));
        }else{
            echo "lala";
        }
    }
});

//delete
$app->get('/admin/delete/article/:article_id', function ($article_id) use ($use) {
    $use->db->exec("delete from do_article where article_id='".$article_id."'");
    $use->db->exec("delete from article where article_id='".$article_id."'");
    $use->app->redirect($use->app->urlFor('admin-article'));
})->name("admin-delete-article");
?>
