<?php
$app->config('debug', true);
$app->log->setEnabled(true);

$app->get('/', function () use ($use) {
    $use->template->prepare('front-home.html');

    $banner = (object) ["result"=>[]];
    $use->app->applyHook('banner', $banner);

    $top_product = (object) ["result"=>[]];
    $use->app->applyHook('top-product', $top_product);

    $category_param = (object) ['results' => []];
    $use->app->applyHook('category', $category_param);

    $top_news = (object) ['results' => []];
    $use->app->applyHook('top-news', $top_news);

    $use->template->param("category", $category_param->results["categories"]);
    $use->template->param('banner', $banner->result);
    $use->template->param('top-product', $top_product->result);
    $use->template->param('top-news', $top_news->result["top-news"]);
    $use->template->param('top-promo', $top_news->result["top-promo"]);

    $use->template->execute();
});

$app->hook('banner', function ($param) use ($use){
    $query = "
        select * from carousel where carousel_status='visible'
    ";
    $select = $use->db->prepare($query);
    $select->execute();
    $result=$select->fetchAll(PDO::FETCH_ASSOC);
    $param->result = $result;
});

$app->hook('top-product', function ($param) use ($use){
    $query = "
        select * from product limit 6
    ";
    $select = $use->db->prepare($query);
    $select->execute();
    $result=$select->fetchAll(PDO::FETCH_ASSOC);
    $param->result = $result;
});

$app->hook('top-news', function ($param) use ($use){
    $query = "
        select * from article where article_type='news' order by article_date desc limit 6
    ";
    $select = $use->db->prepare($query);
    $select->execute();
    $result=$select->fetchAll(PDO::FETCH_ASSOC);
    $param->result["top-news"] = $result;

    $query = "
        select * from article where article_type='promo' order by article_date desc limit 6
    ";
    $select = $use->db->prepare($query);
    $select->execute();
    $result=$select->fetchAll(PDO::FETCH_ASSOC);
    $param->result["top-promo"] = $result;


});


?>
