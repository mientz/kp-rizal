<?php
// view
$app->get('/admin/view/products(/:product_search(/:product_category(/:extra)))', function ($product_search = '$', $product_category = '$', $extra = '$') use ($use) {
    session("user_id");
    if($product_search=='$'){$use->app->redirect($use->app->urlFor('admin-product', array('product_search'=>'@')));}
    if($extra!='$'){$use->app->redirect($use->app->urlFor('admin-product', array('product_search'=>'@', 'product_category'=>$extra)));}
    $use->template->prepare('admin-product.html');

    $product_param = (object) ["product_search"=>$product_search, "product_category"=>$product_category, 'results' => [], 'found'=>true];
    $use->app->applyHook('product', $product_param);

    $category_param = (object) ['results' => []];
    $use->app->applyHook('category', $category_param);

    $use->template->param("users", $use->users);
    $use->template->param("product", $product_param->results["product"]);
    $use->template->param("category", $category_param->results["categories"]);
    $use->template->execute();
})->name('admin-product');
$app->hook('product', function ($param) use ($use) {
    $product_search=$param->product_search;
    $product_category=$param->product_category;
    $categories_query = "";
    $query = "
        select
            p.product_brand, p.product_code, p.product_color, p.product_id, p.product_image,
            p.product_ispopular, p.product_material, p.product_name, p.product_price, p.product_size,
            p.product_view, GROUP_CONCAT(c.category_name SEPARATOR ', ') 'product_category'
        from
            product p, product_category pc, category c
        where
            p.product_id=pc.product_id and c.category_id=pc.category_id
            ".($product_search != '@'  ? "
                and p.product_name ".($product_search != '$'  ? 'like' : 'not like')." CONCAT('%', :product_search, '%') or
                p.product_code ".($product_search != '$'  ? 'like' : 'not like')." CONCAT('%', :product_search, '%') or
                p.product_brand ".($product_search != '$'  ? 'like' : 'not like')." CONCAT('%', :product_search, '%')
            " : '')."
            ".($product_category != '$' ? "and c.category_id='".$product_category."'" : "")."
            group by p.product_id
    ";
    $select = $use->db->prepare($query);
    $select->bindParam(':product_search', $product_search, PDO::PARAM_STR);
    if($select->execute()){
        $result["product"]=$select->fetchAll(PDO::FETCH_ASSOC);
        $result["count"]=$select->rowCount();
        $result["query"]=$query;
        $param->results=$result;
        if($select->rowCount() == 0){
            $param->found=true;
        }else{
            $param->found=false;
        }
        return $result;
    }
});
$app->hook('category', function ($param) use ($use) {
    $select = $use->db->prepare("select * from category where category_parent_id IS NULL");
    $select->execute();
    $result["categories"] = array();
    foreach($select->fetchAll(PDO::FETCH_ASSOC) as $row){

        $select_child1 = $use->db->prepare("select * from category where category_parent_id='".$row["category_id"]."'");
        $select_child1->execute();
        $result_child1 = array();
        foreach($select_child1->fetchAll(PDO::FETCH_ASSOC) as $row_child1){

            $select_child2 = $use->db->prepare("select * from category where category_parent_id='".$row_child1["category_id"]."'");
            $select_child2->execute();
            $result_child2 = array();
            foreach($select_child2->fetchAll(PDO::FETCH_ASSOC) as $row_child2){

                array_push($result_child2, array(
                    "category_id"=>$row_child2["category_id"],
                    "category_name"=>$row_child2["category_name"],
                    "category_parent_id"=>$row_child2["category_parent_id"]
                ));
            }

            array_push($result_child1, array(
                "category_id"=>$row_child1["category_id"],
                "category_name"=>$row_child1["category_name"],
                "category_parent_id"=>$row_child1["category_parent_id"],
                "category_child"=>$result_child2
            ));
        }

        array_push($result["categories"], array(
            "category_id"=>$row["category_id"],
            "category_name"=>$row["category_name"],
            "category_parent_id"=>$row["category_parent_id"],
            "category_child"=>$result_child1
        ));
    }
    $param->results = $result;
});

//insert
$app->get('/admin/add/product', function () use ($use) {
    session("user_id");
    $use->template->prepare('admin-add-product.html');

    $category_param = (object) ['results' => []];
    $use->app->applyHook('category', $category_param);

    $use->template->param("users", $use->users);
    $use->template->param("category", $category_param->results["categories"]);
    $use->template->execute();
})->name("admin-add-product");
$app->post('/admin/add/product', function () use ($use) {
    if(isset($_FILES["product_image"])){
        if ($_FILES["product_image"]["type"] == "image/bmp" || $_FILES["product_image"]["type"] == "image/x-png" || $_FILES["product_image"]["type"] == "image/jpeg"){
            $user_id = $use->app->request->post('user_id');
            $product_name = $use->app->request->post('product_name');
            $product_code = $use->app->request->post('product_code');
            $product_material = $use->app->request->post('product_material');
            $product_color = $use->app->request->post('product_color');
            $product_size = $use->app->request->post('product_size');
            $product_price = $use->app->request->post('product_price');
            $product_category_parent = $use->app->request->post('product_category_parent');
            $product_category_children = $use->app->request->post('product_category_children');
            $product_brand = $use->app->request->post('product_brand');

            $ext = end((explode(".", $_FILES['product_image']['name'])));
            $sourcePath = $_FILES['product_image']['tmp_name'];
            $file = 'product-'.$product_name."-".date("-Y-m-d-H-i-s-").'.'.$ext;
            $targetPath = "public/data/product/".$file; // Target path where file is to be stored
            move_uploaded_file($sourcePath, $targetPath) ; // Moving Uploaded file
            $insert = $use->db->prepare("
                insert into product
                values('', :product_code, :product_name, :product_brand, :product_material, :product_color, :product_size, :product_price,  '0', '0', :product_image)
            ");
            $insert->bindParam(':product_name', $product_name, PDO::PARAM_STR);
            $insert->bindParam(':product_code', $product_code, PDO::PARAM_INT);
            $insert->bindParam(':product_brand', $product_brand, PDO::PARAM_STR);
            $insert->bindParam(':product_material', $product_material, PDO::PARAM_STR);
            $insert->bindParam(':product_color', $product_color, PDO::PARAM_STR);
            $insert->bindParam(':product_size', $product_size, PDO::PARAM_STR);
            $insert->bindParam(':product_price', $product_price, PDO::PARAM_STR);
            $insert->bindParam(':product_image', $file, PDO::PARAM_STR);
            if($insert->execute()){
                $product_id = $use->db->lastInsertId();
                $use->db->exec("insert into do_product values('', '".$user_id."', '".$product_id."', 'add', '".date('m/d/Y h:i:s', time())."')");
                $use->db->exec("insert into product_category values('', '".$product_id."', '".$product_category_parent."')");
                $use->db->exec("insert into product_category values('', '".$product_id."', '".$product_category_children."')");
                $use->app->redirect($use->app->urlFor('admin-product'));
            }else{
                echo "lala";
            }
        }
    }
});
$app->hook('add-product-category', function ($param) use ($use){
    $category_parent = function () use($use){
        $select = $use->db->query("select * from category where category_parent_id IS NULL")->fetchAll(PDO::FETCH_ASSOC);
        return $select;
    };
    $param->category_parent = $category_parent;
});

//update
$app->get('/admin/edit/product/:product_id', function ($product_id) use ($use) {
    session("user_id");
    $use->template->prepare('admin-edit-product.html');

    $category_param = (object) ['results' => []];
    $use->app->applyHook('category', $category_param);

    $data_edit = (object) ['product_id'=>$product_id, "result"=>[]];
    $use->app->applyHook('product_detail', $data_edit);

    $use->template->param("users", $use->users);
    $use->template->param("category", $category_param->results["categories"]);
    $use->template->param("products", $data_edit->result);
    $use->template->execute();
})->name("admin-edit-product");
$app->hook('product_detail', function ($param) use ($use){
    $query = "
        select
            p.product_brand, p.product_code, p.product_color, p.product_id, p.product_image,
            p.product_ispopular, p.product_material, p.product_name, p.product_price, p.product_size,
            p.product_view, GROUP_CONCAT(c.category_name SEPARATOR ', ') 'product_category_name',
            GROUP_CONCAT(c.category_id SEPARATOR ', ') 'product_category'
        from
            product p, product_category pc, category c
        where
            p.product_id=pc.product_id and c.category_id=pc.category_id  and
            p.product_id = '".$param->product_id."'
            group by p.product_id
    ";
    $select = $use->db->prepare($query);
    $select->execute();
    $result=$select->fetchAll(PDO::FETCH_ASSOC);
    $param->result = $result;
});
$app->post('/admin/edit/product', function () use ($use) {
    $user_id = $use->app->request->post('user_id');
    $product_id = $use->app->request->post('product_id');
    $product_name = $use->app->request->post('product_name');
    $product_code = $use->app->request->post('product_code');
    $product_material = $use->app->request->post('product_material');
    $product_color = $use->app->request->post('product_color');
    $product_size = $use->app->request->post('product_size');
    $product_price = $use->app->request->post('product_price');
    $product_category_parent = $use->app->request->post('product_category_parent');
    $product_category_children = $use->app->request->post('product_category_children');
    $product_brand = $use->app->request->post('product_brand');
    $product_ispopular = $use->app->request->post('product_ispopular');
    if(isset($_FILES["product_image"]) && $_FILES['product_image']['tmp_name'] != ""){
        if ($_FILES["product_image"]["type"] == "image/bmp" || $_FILES["product_image"]["type"] == "image/png" || $_FILES["product_image"]["type"] == "image/jpeg"){

            $ext = end((explode(".", $_FILES['product_image']['name'])));
            $sourcePath = $_FILES['product_image']['tmp_name'];
            $file = 'product-'.$product_name."-".date("-Y-m-d-H-i-s-").'.'.$ext;
            $targetPath = "public/data/product/".$file; // Target path where file is to be stored
            move_uploaded_file($sourcePath, $targetPath) ; // Moving Uploaded file
            $insert = $use->db->prepare("
                update product
                set
                    product_code=:product_code,
                    product_name=:product_name,
                    product_brand=:product_brand,
                    product_material=:product_material,
                    product_color=:product_color,
                    product_size=:product_size,
                    product_price=:product_price,
                    product_ispopular=:product_ispopular,
                    product_image=:product_image

                where
                    product_id=:product_id
            ");
            $insert->bindParam(':product_id', $product_id, PDO::PARAM_INT);
            $insert->bindParam(':product_name', $product_name, PDO::PARAM_STR);
            $insert->bindParam(':product_code', $product_code, PDO::PARAM_INT);
            $insert->bindParam(':product_brand', $product_brand, PDO::PARAM_STR);
            $insert->bindParam(':product_material', $product_material, PDO::PARAM_STR);
            $insert->bindParam(':product_color', $product_color, PDO::PARAM_STR);
            $insert->bindParam(':product_size', $product_size, PDO::PARAM_STR);
            $insert->bindParam(':product_price', $product_price, PDO::PARAM_STR);
            $insert->bindParam(':product_ispopular', $product_ispopular, PDO::PARAM_INT);
            $insert->bindParam(':product_image', $file, PDO::PARAM_STR);
            if($insert->execute()){
                $use->db->exec("insert into do_product values('', '".$user_id."', '".$product_id."', 'edit', '".date('Y-m-d h:i:s', time())."')");
                $use->db->exec("delete from product where product_id='".$product_id."'");
                $use->db->exec("insert into product_category values('', '".$product_id."', '".$product_category_parent."')");
                $use->db->exec("insert into product_category values('', '".$product_id."', '".$product_category_children."')");
                $use->app->redirect($use->app->urlFor('admin-product'));
            }else{
                echo "lala";
            }
        }
    }else{
        $insert = $use->db->prepare("
                update product
                set
                    product_code=:product_code,
                    product_name=:product_name,
                    product_brand=:product_brand,
                    product_material=:product_material,
                    product_color=:product_color,
                    product_size=:product_size,
                    product_price=:product_price,
                    product_ispopular=:product_ispopular

                where
                    product_id=:product_id
            ");
        $insert->bindParam(':product_id', $product_id, PDO::PARAM_INT);
        $insert->bindParam(':product_name', $product_name, PDO::PARAM_STR);
        $insert->bindParam(':product_code', $product_code, PDO::PARAM_INT);
        $insert->bindParam(':product_brand', $product_brand, PDO::PARAM_STR);
        $insert->bindParam(':product_material', $product_material, PDO::PARAM_STR);
        $insert->bindParam(':product_color', $product_color, PDO::PARAM_STR);
        $insert->bindParam(':product_size', $product_size, PDO::PARAM_STR);
        $insert->bindParam(':product_price', $product_price, PDO::PARAM_STR);
        $insert->bindParam(':product_ispopular', $product_ispopular, PDO::PARAM_INT);
        if($insert->execute()){
            $use->db->exec("insert into do_product values('', '".$user_id."', '".$product_id."', 'edit', '".date('Y-m-d h:i:s', time())."')");
            $use->db->exec("delete from product_category where product_id='".$product_id."'");
            $use->db->exec("insert into product_category values('', '".$product_id."', '".$product_category_parent."')");
            $use->db->exec("insert into product_category values('', '".$product_id."', '".$product_category_children."')");
            $use->app->redirect($use->app->urlFor('admin-product'));
        }else{
            echo "lala";
        }
    }
});

//delete
$app->get('/admin/delete/product/:product_id', function ($product_id) use ($use) {
    $use->db->exec("delete from do_product where product_id='".$product_id."'");
    $use->db->exec("delete from product_category where product_id='".$product_id."'");
    $use->db->exec("delete from product where product_id='".$product_id."'");
    $use->app->redirect($use->app->urlFor('admin-product'));
})->name("admin-delete-product");

//helper
//category

?>
