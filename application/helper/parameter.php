<?php
class parameter{
    public $app;
    public $template;
    public $db;
    public $users;
    public $mail;
    public $req;
    public function __construct($app, $template, $db, $users, $mail){
        $this->app = $app;
        $this->template = $template;
        $this->db = $db;
        $this->users = $users;
        $this->mail = $mail;
        $this->req = $app->request;
    }
}
?>
