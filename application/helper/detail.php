<?php
$user_detail = function ($user_id) use ($db){
    $select = $db->prepare("select * from user where user_id=:user_id");
    $select->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $select->execute();
    return $select->fetch(PDO::FETCH_ASSOC);
};
?>
