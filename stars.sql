-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2016 at 08:25 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stars`
--
CREATE DATABASE IF NOT EXISTS `stars` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `stars`;

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,
  `article_title` longtext NOT NULL,
  `article_date` date NOT NULL,
  `article_text` longtext,
  `article_image` longtext,
  `article_status` varchar(255) NOT NULL,
  `article_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` longtext,
  `contact_city` longtext,
  `contact_address` longtext,
  `contact_email` longtext,
  `contact_telp` longtext,
  `contact_messages` longtext,
  `contact_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `do_article`
--

CREATE TABLE `do_article` (
  `doarticle_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `doarticle_what` varchar(255) NOT NULL,
  `doarticle_when` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `do_contact`
--

CREATE TABLE `do_contact` (
  `docontact_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `docontact_what` varchar(255) NOT NULL,
  `docontact_when` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `do_gallery`
--

CREATE TABLE `do_gallery` (
  `dogallery_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `dogallery_what` varchar(255) NOT NULL,
  `dogallery_when` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `do_product`
--

CREATE TABLE `do_product` (
  `doproduct_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `doproduct_what` varchar(255) NOT NULL,
  `doproduct_when` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL,
  `gallery_name` longtext,
  `gallery_file` longtext NOT NULL,
  `gallery_uploaded` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_code` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `product_material` varchar(100) DEFAULT NULL,
  `product_color` varchar(100) DEFAULT NULL,
  `product_size` varchar(100) DEFAULT NULL,
  `product_prize` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fulname` varchar(100) NOT NULL,
  `image` longtext NOT NULL,
  `activated` tinyint(1) NOT NULL,
  `permission` varchar(255) NOT NULL DEFAULT 'member',
  `last_login` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `do_article`
--
ALTER TABLE `do_article`
  ADD PRIMARY KEY (`doarticle_id`),
  ADD KEY `fk_user_do_promo_user_1` (`user_id`),
  ADD KEY `fk_user_do_promo_promo_1` (`article_id`);

--
-- Indexes for table `do_contact`
--
ALTER TABLE `do_contact`
  ADD PRIMARY KEY (`docontact_id`),
  ADD KEY `fk_do_contact_message_1` (`contact_id`),
  ADD KEY `fk_do_contact_user_1` (`user_id`);

--
-- Indexes for table `do_gallery`
--
ALTER TABLE `do_gallery`
  ADD PRIMARY KEY (`dogallery_id`),
  ADD KEY `fk_do_gallery_user_1` (`user_id`),
  ADD KEY `fk_do_gallery_gallery_1` (`gallery_id`);

--
-- Indexes for table `do_product`
--
ALTER TABLE `do_product`
  ADD PRIMARY KEY (`doproduct_id`),
  ADD KEY `fk_user_do_user_1` (`user_id`),
  ADD KEY `fk_user_do_product_1` (`product_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `do_article`
--
ALTER TABLE `do_article`
  MODIFY `doarticle_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `do_contact`
--
ALTER TABLE `do_contact`
  MODIFY `docontact_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `do_gallery`
--
ALTER TABLE `do_gallery`
  MODIFY `dogallery_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `do_product`
--
ALTER TABLE `do_product`
  MODIFY `doproduct_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `do_article`
--
ALTER TABLE `do_article`
  ADD CONSTRAINT `fk_user_do_promo_promo_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`),
  ADD CONSTRAINT `fk_user_do_promo_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `do_contact`
--
ALTER TABLE `do_contact`
  ADD CONSTRAINT `fk_do_contact_message_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`contact_id`),
  ADD CONSTRAINT `fk_do_contact_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `do_gallery`
--
ALTER TABLE `do_gallery`
  ADD CONSTRAINT `fk_do_gallery_gallery_1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`gallery_id`),
  ADD CONSTRAINT `fk_do_gallery_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `do_product`
--
ALTER TABLE `do_product`
  ADD CONSTRAINT `fk_user_do_product_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `fk_user_do_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
